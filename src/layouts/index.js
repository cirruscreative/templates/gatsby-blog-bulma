import React from 'react'
import Link from 'gatsby-link'

// Import Bulma & Custome Sass
import '../assets/sass/main.sass'

class Template extends React.Component {
  render() {
    const { location, children } = this.props
    let header

    let rootPath = `/`
    if (typeof __PREFIX_PATHS__ !== `undefined` && __PREFIX_PATHS__) {
      rootPath = __PATH_PREFIX__ + `/`
    }

    if (location.pathname === rootPath) {
      header = (
        <h1 className="title">
          <Link
            to={'/'}
          >
            Gatsby Starter Blog
          </Link>
        </h1>
      )
    } else {
      header = (
        <h3 className="subtitle">
          <Link
            to={'/'}
          >
            Gatsby Starter Blog
          </Link>
        </h3>
      )
    }
    return (
      <div className="section">
        {header}
        {children()}
      </div>
    )
  }
}

export default Template
