# Gatsby Blog & Bulma Starter

Gatsby starter for creating a blog with bulma

Install this starter by cloning the repo, then installing all dependancies:
```powershell
git clone https://gitlab.com/cirruscreative/templates/gatsby-blog-bulma your-folder
cd your-folder
yarn (or npm i)
```

## Running in development

`gatsby develop`

## Building

`gatsby build`
